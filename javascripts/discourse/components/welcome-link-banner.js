import Component from "@ember/component";
import discourseComputed from "discourse-common/utils/decorators";
import { inject as service } from "@ember/service";
import { action, computed } from "@ember/object";
import { defaultHomepage } from "discourse/lib/utilities";
import showModal from "discourse/lib/show-modal";
import EditSpaceModal from "discourse/plugins/meeet-main/discourse/components/modal/edit-space";

export default Component.extend({
  router: service(),
  modal: service(),
  isInModal: true,
  bannerLinks: computed(function () {
    return JSON.parse(settings.banner_links);
  }),
  @action
  openModal(type) {
    const newCategory = Discourse.__container__.lookup("route:new-category");

    if (this.currentUser) {
      this.modal.show(EditSpaceModal, {
        // showModal("edit-space").setProperties({
        model: {
          category: newCategory.newCategoryWithPermissions(
            newCategory.defaultGroupPermissions()
          ),
          activeTab: type,
        },
      });
      return;
    }
  },
  @discourseComputed("currentUser")
  showTrust(currentUser) {
    return (
      (currentUser && currentUser.trust_level <= settings.max_trust_level) ||
      (!currentUser && !settings.hide_for_anon)
    );
  },

  @discourseComputed("currentUser")
  hideStaff(currentUser) {
    return currentUser && currentUser.staff && settings.hide_for_staff;
  },

  @discourseComputed(
    "router.currentRouteName",
    "router.currentURL",
    "isInModal"
  )
  showHere(currentRouteName, currentURL, isInModal) {
    if (isInModal) {
      return isInModal;
    }

    if (settings.show_on === "all") {
      return true;
    }

    if (settings.show_on === currentRouteName) {
      return true;
    }
    if (settings.show_on === "homepage") {
      return (
        currentRouteName == `discovery.${defaultHomepage()}` ||
        currentRouteName == defaultHomepage()
      );
    }

    return currentRouteName.indexOf(settings.show_on) > -1;
  },
});
